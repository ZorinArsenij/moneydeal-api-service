module gitlab.com/ZorinArsenij/moneydeal-api-service

go 1.13

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/google/wire v0.3.0
	github.com/mailru/easyjson v0.7.0
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/ZorinArsenij/authentication-service v0.0.0-20191110092701-bc9960d53270
	gitlab.com/ZorinArsenij/moneydeal-database-service v0.0.0-20191022092421-3461dabbb490
	google.golang.org/grpc v1.24.0
)
