package entity

//go:generate easyjson account.go

// AccountInfo store information about account
// easyjson:json
type AccountInfo struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Phone     string `json:"phone"`
}

// Account store account id and info
// easyjson:json
type Account struct {
	AccountInfo
	ID    string `json:"id"`
	Token string `json:"token"`
}
