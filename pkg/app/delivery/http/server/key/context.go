package key

type contextKey struct {
	name string
}

var (
	ContextLogger = contextKey{
		name: "logger",
	}
	ContextID = contextKey{
		name: "id",
	}
)
