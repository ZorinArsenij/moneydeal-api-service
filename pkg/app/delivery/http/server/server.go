package server

import (
	"net/http"

	"github.com/mailru/easyjson"
	"github.com/sirupsen/logrus"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/entity"

	"github.com/go-chi/chi"
)

type AccountUsecase interface {
	Register(phone string) (string, error)
	GetIDByPhone(phone string) (string, error)
	UpdateAccountInfo(info *entity.Account) error
	BlockPhone(phone string) error
	UnblockPhone(phone string) error
	GetByPhone(phone string) (*entity.Account, error)
	GetByID(id string) (*entity.Account, error)

	GetIDByToken(token string) (string, error)
	CreateToken(userID, tokenFCM string) (string, error)
	DeleteToken(token string) error
}

type Server struct {
	router         chi.Router
	accountUsecase AccountUsecase
	log            *logrus.Logger
}

func New(acc AccountUsecase) Server {
	s := Server{
		router:         chi.NewRouter(),
		accountUsecase: acc,
		log:            logrus.New(),
	}
	s.middlewares()
	s.routes()

	return s
}

func (s *Server) routes() {
	s.router.With(s.Auth).Method(http.MethodPut, "/api/v1/account", s.HandleAccountUpdate())
	s.router.With(s.Auth).Method(http.MethodGet, "/api/v1/account", s.HandleAccountInfo())
	s.router.Method(http.MethodPost, "/api/v1/login", s.HandleLogin())
}

func (s *Server) middlewares() {
	s.router.Use(s.AccessLogger)
}

func (s Server) Start(addr string) error {
	server := &http.Server{Addr: addr, Handler: s.router}
	err := server.ListenAndServe()
	return err
}

func (s Server) decode(r *http.Request, v easyjson.Unmarshaler) error {
	defer r.Body.Close()
	return easyjson.UnmarshalFromReader(r.Body, v)
}

func (s Server) respond(w http.ResponseWriter, data easyjson.Marshaler, status int) {
	w.WriteHeader(status)
	if data != nil {
		if _, _, err := easyjson.MarshalToHTTPResponseWriter(data, w); err != nil {
			return
		}
	}
}
