// Code generated by easyjson for marshaling/unmarshaling. DO NOT EDIT.

package server

import (
	json "encoding/json"
	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
)

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer(in *jlexer.Lexer, out *responseLogin) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "id":
			out.ID = string(in.String())
		case "token":
			out.Token = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer(out *jwriter.Writer, in responseLogin) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"id\":"
		out.RawString(prefix[1:])
		out.String(string(in.ID))
	}
	{
		const prefix string = ",\"token\":"
		out.RawString(prefix)
		out.String(string(in.Token))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v responseLogin) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v responseLogin) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *responseLogin) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *responseLogin) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer(l, v)
}
func easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer1(in *jlexer.Lexer, out *responseGoogle) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "phoneNumber":
			out.Phone = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer1(out *jwriter.Writer, in responseGoogle) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"phoneNumber\":"
		out.RawString(prefix[1:])
		out.String(string(in.Phone))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v responseGoogle) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer1(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v responseGoogle) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer1(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *responseGoogle) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer1(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *responseGoogle) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer1(l, v)
}
func easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer2(in *jlexer.Lexer, out *responseAccountInfo) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "id":
			out.ID = string(in.String())
		case "first_name":
			out.FirstName = string(in.String())
		case "last_name":
			out.LastName = string(in.String())
		case "phone":
			out.Phone = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer2(out *jwriter.Writer, in responseAccountInfo) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"id\":"
		out.RawString(prefix[1:])
		out.String(string(in.ID))
	}
	if in.FirstName != "" {
		const prefix string = ",\"first_name\":"
		out.RawString(prefix)
		out.String(string(in.FirstName))
	}
	if in.LastName != "" {
		const prefix string = ",\"last_name\":"
		out.RawString(prefix)
		out.String(string(in.LastName))
	}
	if in.Phone != "" {
		const prefix string = ",\"phone\":"
		out.RawString(prefix)
		out.String(string(in.Phone))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v responseAccountInfo) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer2(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v responseAccountInfo) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer2(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *responseAccountInfo) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer2(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *responseAccountInfo) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer2(l, v)
}
func easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer3(in *jlexer.Lexer, out *requestUpdate) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "first_name":
			out.FirstName = string(in.String())
		case "last_name":
			out.LastName = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer3(out *jwriter.Writer, in requestUpdate) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"first_name\":"
		out.RawString(prefix[1:])
		out.String(string(in.FirstName))
	}
	{
		const prefix string = ",\"last_name\":"
		out.RawString(prefix)
		out.String(string(in.LastName))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v requestUpdate) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer3(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v requestUpdate) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer3(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *requestUpdate) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer3(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *requestUpdate) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer3(l, v)
}
func easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer4(in *jlexer.Lexer, out *requestLogin) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "phone":
			out.Phone = string(in.String())
		case "tokenFCM":
			out.TokenFCM = string(in.String())
		case "google_token":
			out.TokenGoogle = string(in.String())
		case "code":
			out.Code = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer4(out *jwriter.Writer, in requestLogin) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"phone\":"
		out.RawString(prefix[1:])
		out.String(string(in.Phone))
	}
	{
		const prefix string = ",\"tokenFCM\":"
		out.RawString(prefix)
		out.String(string(in.TokenFCM))
	}
	{
		const prefix string = ",\"google_token\":"
		out.RawString(prefix)
		out.String(string(in.TokenGoogle))
	}
	{
		const prefix string = ",\"code\":"
		out.RawString(prefix)
		out.String(string(in.Code))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v requestLogin) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer4(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v requestLogin) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer4(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *requestLogin) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer4(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *requestLogin) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer4(l, v)
}
func easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer5(in *jlexer.Lexer, out *requestGoogle) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "sessionInfo":
			out.TokenGoogle = string(in.String())
		case "code":
			out.Code = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer5(out *jwriter.Writer, in requestGoogle) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"sessionInfo\":"
		out.RawString(prefix[1:])
		out.String(string(in.TokenGoogle))
	}
	{
		const prefix string = ",\"code\":"
		out.RawString(prefix)
		out.String(string(in.Code))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v requestGoogle) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer5(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v requestGoogle) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson349b126bEncodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer5(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *requestGoogle) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer5(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *requestGoogle) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson349b126bDecodeGitlabComZorinArsenijMoneydealApiServicePkgAppDeliveryHttpServer5(l, v)
}
