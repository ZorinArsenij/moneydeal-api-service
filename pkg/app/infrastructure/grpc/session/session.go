package session

import (
	"context"

	"google.golang.org/grpc/codes"

	service "gitlab.com/ZorinArsenij/authentication-service/pkg/app/delivery/grpc/session"
	"gitlab.com/ZorinArsenij/authentication-service/pkg/errors"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/internal/config"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/entity"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
)

// Repository process requests to session storage
type Repository struct {
	client service.SessionClient
}

// NewRepository create instance of Repository
func NewRepository(cfg config.Config) (Repository, error) {
	conn, err := grpc.Dial(cfg.SessionServiceAddr, grpc.WithInsecure())
	if err != nil {
		return Repository{}, err
	}

	return Repository{
		client: service.NewSessionClient(conn),
	}, nil
}

// Create add new user session
func (r Repository) Create(userID, tokenFCM string) (string, error) {
	req := &service.SessionCreateRequest{
		UserID:   userID,
		TokenFCM: tokenFCM,
	}
	resp, err := r.client.Create(context.Background(), req)
	if err != nil {
		return "", err
	}
	return resp.Token, nil
}

// Get find user session by token
func (r Repository) Get(token string) (*entity.Session, error) {
	req := &service.SessionRequest{
		Token: token,
	}
	resp, err := r.client.Get(context.Background(), req)
	if err != nil {
		e, _ := status.FromError(err)
		if e.Code() == codes.NotFound {
			return nil, errors.ErrNotFound
		}

		return nil, err
	}

	return &entity.Session{
		UserID:   resp.UserID,
		Token:    resp.Token,
		TokenFCM: resp.TokenFCM,
	}, nil
}

// Delete remove user session by token
func (r Repository) Delete(token string) error {
	req := &service.SessionRequest{
		Token: token,
	}
	_, err := r.client.Delete(context.Background(), req)
	return err
}
