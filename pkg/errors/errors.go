package errors

import "errors"

var (
	ErrNotFound      = errors.New("not found")
	ErrInvalidCode   = errors.New("invalid code")
	ErrRequestFailed = errors.New("request failed")
)
