//+build wireinject

package wire

//go:generate wire

import (
	"gitlab.com/ZorinArsenij/moneydeal-api-service/internal/config"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/delivery/http/server"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/infrastructure/grpc/session"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/infrastructure/grpc/storage"

	"github.com/google/wire"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/usecase"
)

func InitializeServer(cfg config.Config) (server.Server, error) {
	wire.Build(
		server.New,
		usecase.NewAccountUsecase,
		wire.Bind(new(usecase.StorageRepository), new(storage.Repository)),
		wire.Bind(new(usecase.StorageSession), new(session.Repository)),
		wire.Bind(new(server.AccountUsecase), new(usecase.Account)),
		session.NewRepository,
		storage.NewRepository)
	return server.Server{}, nil
}
