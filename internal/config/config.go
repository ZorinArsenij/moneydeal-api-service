package config

type Config struct {
	StorageServiceAddr string
	SessionServiceAddr string
}
